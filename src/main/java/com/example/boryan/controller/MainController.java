package com.example.boryan.controller;

import java.util.*;

import com.example.boryan.domain.User;
import com.example.boryan.repos.MessageRepository;
import com.example.boryan.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class MainController {
    @Autowired
    private MessageRepository messageRepository;

    @GetMapping("/")
    public String greeting(Map <String,Object> model){
        return "greeting";
    }
    @GetMapping("/main")
    public String getAllUsers(Map<String, Object> model) {
        Iterable <Message> messages = messageRepository.findAll();
        model.put("messages",messages);
        return "main";
    }

    @PostMapping("/main")
    public String add(
            @AuthenticationPrincipal User user,
            @RequestParam String text,
            @RequestParam String tag, Map<String, Object> model){

        System.err.println(model.size());
        Message message = new Message(text,tag, user);
       messageRepository.save(message);
       Iterable <Message> msg = messageRepository.findAll();
        //System.err.println(((List<Message>)(msg)).size());
       model.put("messages",msg);
       return "main";
    }
    @PostMapping("/filter")
    public String filter(@RequestParam String tag, Map<String,Object> model){
        Iterable <Message> filteredMessages;
        if (tag != null && !tag.isEmpty()) {
            filteredMessages = messageRepository.findByTag(tag);
        } else{
            filteredMessages = messageRepository.findAll();
        }
       // System.err.println(((List<Message>)(filteredMessages)).size());
        model.put("messages", filteredMessages);
        return "main";
    }
}
