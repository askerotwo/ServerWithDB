package com.example.boryan.repos;

import com.example.boryan.domain.Message;
import org.springframework.data.repository.CrudRepository;
import java.util.*;

public interface MessageRepository extends CrudRepository<Message,Long> {
    List <Message> findByTag(String tag);
}
